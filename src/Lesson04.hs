{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson04 (main) where

import Control.Applicative
import Control.Monad hiding (mapM_)
import Data.Foldable hiding (elem)
import Data.Maybe
import Data.Monoid
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import qualified SDL
import SDL.Vect
import Prelude hiding (any, mapM_)

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

-- Load image at specified path
loadBMP :: FilePath -> IO SDL.Surface
loadBMP path = getDataFileName path >>= SDL.loadBMP

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Get window surface
  screenSurface <- SDL.getWindowSurface window

  -- Load default surface
  surfaceDefault <- loadBMP "press.bmp"
  -- Load up surface
  surfaceUp <- loadBMP "up.bmp"
  -- Load down surface
  surfaceDown <- loadBMP "down.bmp"
  -- Load left surface
  surfaceLeft <- loadBMP "left.bmp"
  -- Load right surface
  surfaceRight <- loadBMP "right.bmp"

  let loop oldSurface = do
        -- Pools events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events

        -- Input handler
        let handleInput = \case
              -- User presses a key
              SDL.KeyboardEvent e | SDL.keyboardEventKeyMotion e == SDL.Pressed ->
                -- Select surfaces based on key press
                case SDL.keysymKeycode $ SDL.keyboardEventKeysym e of
                  SDL.KeycodeUp -> Last $ Just surfaceUp
                  SDL.KeycodeDown -> Last $ Just surfaceDown
                  SDL.KeycodeRight -> Last $ Just surfaceRight
                  SDL.KeycodeLeft -> Last $ Just surfaceLeft
                  _ -> mempty
              _ -> mempty

        -- Current surface based on last key press
        let currentSurface = fromMaybe oldSurface $ getLast $ foldMap handleInput events

        -- Apply the current image
        SDL.surfaceBlit currentSurface Nothing screenSurface Nothing

        -- Updates the surface
        SDL.updateWindowSurface window

        -- Continue loop unless user requests quit
        unless quit (loop currentSurface)

  -- Start loop
  loop surfaceDefault

  -- Deallocate surfaces
  mapM_ SDL.freeSurface [surfaceDefault, surfaceUp, surfaceDown, surfaceRight, surfaceLeft]

  -- Destroy window
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
