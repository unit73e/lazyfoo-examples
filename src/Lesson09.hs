{-# LANGUAGE OverloadedStrings #-}

module Lesson09 (main) where

import Control.Monad
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Creates renderer for window
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load image at specified path
  textureSurface <- getDataFileName "viewport.png" >>= SDL.Image.load

  -- Create texture from surface pixels
  texture <- SDL.createTextureFromSurface renderer textureSurface

  -- Get rid of old loaded surface
  SDL.freeSurface textureSurface

  -- Shows 3 viewports until the user closes the window
  let loop = do
        -- Pools events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Top left viewport 
        let topLeftOrigin = P $ V2 0 0
        let topLeftSize = V2 (screenWidth `div` 2) (screenHeight `div` 2)
        let topLeftViewPort = SDL.Rectangle topLeftOrigin topLeftSize
        SDL.rendererViewport renderer $= Just topLeftViewPort

        -- Render texture to screen
        SDL.copy renderer texture Nothing Nothing

        -- Top right viewport 
        let topRightOrigin = P $ V2 (screenWidth `div` 2) 0
        let topRightSize = V2 (screenWidth `div` 2) (screenHeight `div` 2)
        let topRightViewport = SDL.Rectangle topRightOrigin topRightSize
        SDL.rendererViewport renderer $= Just topRightViewport

        -- Render texture to screen
        SDL.copy renderer texture Nothing Nothing

        -- Bottom viewport
        let bottomOrigin = P $ V2 0 (screenHeight `div` 2)
        let bottomSize = V2 screenWidth (screenHeight `div` 2)
        let bottomViewport = SDL.Rectangle bottomOrigin bottomSize
        SDL.rendererViewport renderer $= Just bottomViewport

        -- Render texture to screen
        SDL.copy renderer texture Nothing Nothing

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit loop

  -- Start loop
  loop

  -- Free loaded image
  SDL.destroyTexture texture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
