{-# LANGUAGE OverloadedStrings #-}

module Lesson01 (main) where

import Control.Concurrent (threadDelay)
import Foreign.C.Types
import qualified SDL
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

main :: IO ()
main = do
  -- Initializes SDL
  SDL.initialize [SDL.InitVideo]

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Get window surface
  screenSurface <- SDL.getWindowSurface window

  -- Fill the surface white
  let white = V4 maxBound maxBound maxBound maxBound
  SDL.surfaceFillRect screenSurface Nothing white

  -- Update the surface
  SDL.updateWindowSurface window

  -- Wait two seconds
  threadDelay 2000000

  -- Destroy window
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
