{-# LANGUAGE OverloadedStrings #-}

module Lesson03 (main) where

import Control.Monad
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import qualified SDL
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Get window surface
  screenSurface <- SDL.getWindowSurface window

  -- Load splash image
  xOut <- getDataFileName "x.bmp" >>= SDL.loadBMP

  let loop = do
        -- Pool events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events

        -- Apply the image
        SDL.surfaceBlit xOut Nothing screenSurface Nothing

        -- Update the surface
        SDL.updateWindowSurface window

        -- Continue loop unless user requests quit
        unless quit loop

  -- Start loop
  loop

  -- Deallocate surface
  SDL.freeSurface xOut

  -- Destroy window
  SDL.destroyWindow window

  -- Quite SDL subsystems
  SDL.quit
