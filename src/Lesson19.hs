{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

module Lesson19 (main) where

import Control.Monad hiding (mapM_)
import Control.Monad.IO.Class (MonadIO)
import Data.Int
import Data.Maybe
import Data.Monoid
import qualified Data.Vector as V
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect
import Prelude hiding (any, mapM_)

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

-- Analog joystick dead zone
joystickDeadZone :: Int16
joystickDeadZone = 8000

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture ::
  SDL.Renderer ->
  Texture ->
  Point V2 CInt ->
  Maybe (SDL.Rectangle CInt) ->
  Maybe CDouble ->
  Maybe (Point V2 CInt) ->
  Maybe (V2 Bool) ->
  IO ()
renderTexture r (Texture t size) xy clip theta center flips =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
   in -- Render to screen
      SDL.copyEx
        r
        t
        clip
        (Just $ SDL.Rectangle xy dstSize)
        (fromMaybe 0 theta)
        center
        (fromMaybe (pure False) flips)

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

textureSize :: Texture -> V2 CInt
textureSize (Texture _ sz) = sz

getJoystick :: IO SDL.Joystick
getJoystick = do
  joysticks <- SDL.availableJoysticks
  joystick <-
    if V.length joysticks == 0
      then error "No joysticks connected!"
      else return (joysticks V.! 0)
  SDL.openJoystick joystick

main :: IO ()
main = do
  -- Initialize DF
  SDL.initialize [SDL.InitVideo, SDL.InitJoystick]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create vsynced renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedVSyncRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load texture
  arrowTexture <- loadTexture renderer "arrow.png"

  joystick <- getJoystick
  joystickID <- SDL.getJoystickID joystick

  let loop (xDir', yDir') = do
        events <- SDL.pollEvents

        let handleInput =
              \case
                SDL.QuitEvent -> (Any True, mempty)
                SDL.JoyAxisEvent e ->
                  if SDL.joyAxisEventWhich e == joystickID
                    then (\x -> (mempty, Last $ Just x)) $
                      case SDL.joyAxisEventAxis e of
                        0 ->
                          if
                              | SDL.joyAxisEventValue e < - joystickDeadZone ->
                                (- 1, yDir')
                              | SDL.joyAxisEventValue e > joystickDeadZone -> (1, yDir')
                              | otherwise -> (0, yDir')
                        1 ->
                          if
                              | SDL.joyAxisEventValue e < - joystickDeadZone ->
                                (xDir', - 1)
                              | SDL.joyAxisEventValue e > joystickDeadZone -> (xDir', 1)
                              | otherwise -> (xDir', 0)
                        _ -> (xDir', yDir')
                    else mempty
                _ -> mempty

        let (Any quit, Last newDir) = foldMap (handleInput . SDL.eventPayload) events

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Calculate angle
        let dir@(xDir, yDir) = fromMaybe (xDir', yDir') newDir
            phi =
              if xDir == 0 && yDir == 0
                then 0
                else atan2 yDir xDir * (180.0 / pi)

        -- Render joystick 8 way angle
        let screenSize = V2 screenWidth screenHeight
        let arrowSize = textureSize arrowTexture
        let position = P $ fmap (`div` 2) screenSize - fmap (`div` 2) arrowSize
        renderTexture renderer arrowTexture position Nothing (Just phi) Nothing Nothing

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit $ loop dir

  -- Start loop
  loop (0, 0)

  -- Free loaded images
  freeTexture arrowTexture

  -- Close game controller
  SDL.closeJoystick joystick

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.Image.quit
  SDL.quit
