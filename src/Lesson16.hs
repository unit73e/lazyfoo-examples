{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson16 (main) where

import Control.Applicative
import Control.Monad
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Font
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

black = V4 0 0 0 0

loadText :: SDL.Renderer -> SDL.Font.Color -> T.Text -> IO Texture
loadText r color text = do
  -- Open the font
  fontPath <- getDataFileName "lazy.ttf"
  font <- SDL.Font.load fontPath 28
  -- Render text
  surface <- SDL.Font.solid font color text
  -- Free font
  SDL.Font.free font
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture :: SDL.Renderer -> Texture -> Point V2 CInt -> Maybe (SDL.Rectangle CInt) -> IO ()
renderTexture r (Texture t size) xy clip =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      dstRect = Just $ SDL.Rectangle xy dstSize
   in -- Render to screen
      SDL.copy r t clip dstRect

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

textureSize :: Texture -> V2 CInt
textureSize (Texture _ sz) = sz

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Initialize SDL font
  SDL.Font.initialize

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  window <-
    SDL.createWindow
      "SDL Tutorial"
      SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  SDL.showWindow window

  -- Creates an hardware accelerated renderer
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Loads text
  textTexture <- loadText renderer black "The quick brown fox jumps over the lazy dog"

  let loop = do
        -- Pools events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events

        -- Clears the window
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render current frame
        let textSize = textureSize textTexture
        let position = P $ fmap (`div` 2) (V2 screenWidth screenHeight) - fmap (`div` 2) textSize
        renderTexture renderer textTexture position Nothing

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit loop

  -- Start loop
  loop

  -- Free text
  freeTexture textTexture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.Font.quit
  SDL.quit
