{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson22 (main) where

import Control.Lens
import Control.Monad hiding (mapM_)
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable hiding (elem)
import Data.Monoid
import qualified Data.Text as T
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Font
import SDL.Vect
import Prelude hiding (any, mapM_)
import Data.Maybe

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

black = V4 0 0 0 0

loadText :: SDL.Renderer -> SDL.Font.Color -> T.Text -> IO Texture
loadText r color text = do
  -- Open the font
  fontPath <- getDataFileName "lazy.ttf"
  font <- SDL.Font.load fontPath 28
  -- Render text
  surface <- SDL.Font.solid font color text
  -- Free font
  SDL.Font.free font
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture :: SDL.Renderer -> Texture -> Point V2 CInt -> Maybe (SDL.Rectangle CInt) -> IO ()
renderTexture r (Texture t size) xy clip =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      dstRect = Just $ SDL.Rectangle xy dstSize
   in -- Render to screen
      SDL.copy r t clip dstRect

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

textureSize :: Texture -> V2 CInt
textureSize (Texture _ sz) = sz

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Initialize SDL font
  SDL.Font.initialize

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create vsynced renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedVSyncRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Loads text
  promptedTextTexture <- loadText renderer black "Press Enter to Reset Start Time."

  let loop oldStartTime = do
        -- Pools events
        events <- SDL.pollEvents

        -- Get ticks
        ticks <- SDL.ticks

        let handleInput = \case
              -- Set quit
              SDL.QuitEvent -> (Any True, mempty)
              -- User presses a key
              SDL.KeyboardEvent e | SDL.keyboardEventKeyMotion e == SDL.Pressed ->
                -- Select surfaces based on key press
                case SDL.keysymKeycode $ SDL.keyboardEventKeysym e of
                  SDL.KeycodeReturn -> (mempty, Last $ Just ticks)
                  _ -> mempty
              _ -> mempty

        let (Any quit, Last newStartTime) = foldMap (handleInput . SDL.eventPayload) events

        -- Clears the window
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render current frame
        let promptedTextSize = textureSize promptedTextTexture
        let promptedTextPosition = P $ V2 ((screenWidth - promptedTextSize ^. _x) `div` 2) 0
        renderTexture renderer promptedTextTexture promptedTextPosition Nothing

        let startTime = fromMaybe oldStartTime newStartTime
        let timeText = "Milliseconds since start time " `T.append` (T.pack . show) (ticks - startTime)
        timeTexture <- loadText renderer black timeText
        let textSize = textureSize timeTexture
        let position = P $ fmap (`div` 2) (V2 screenWidth screenHeight) - fmap (`div` 2) textSize
        renderTexture renderer timeTexture position Nothing

        -- Update screen
        SDL.present renderer

        -- Free time texture
        freeTexture timeTexture

        -- Continue loop unless user requests quit
        unless quit $ loop startTime

  -- Start loop
  loop 0

  -- Free loaded images
  freeTexture promptedTextTexture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.Font.quit
  SDL.quit
