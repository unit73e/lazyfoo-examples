{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Lesson13 (main) where

import Control.Monad
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable
import Data.Monoid
import Data.Word
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture :: SDL.Renderer -> Texture -> Point V2 CInt -> Maybe (SDL.Rectangle CInt) -> IO ()
renderTexture r (Texture t size) xy clip =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      dstRect = Just $ SDL.Rectangle xy dstSize
   in -- Render to screen
      SDL.copy r t clip dstRect

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

-- Modulate texture alpha
setTextureAlpha :: Texture -> Word8 -> IO ()
setTextureAlpha (Texture t _) rgb = SDL.textureAlphaMod t $= rgb

-- Set blending function
setTextureBlendMode :: Texture -> SDL.BlendMode -> IO ()
setTextureBlendMode (Texture t _) bm = SDL.textureBlendMode t $= bm

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create renderer for window
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load front alpha texture
  modulatedTexture <- loadTexture renderer "fadeout.png"

  -- Set standard alpha blending
  setTextureBlendMode modulatedTexture SDL.BlendAlphaBlend

  -- Load background texture
  backgroundTexture <- loadTexture renderer "fadein.png"

  let loop alpha = do
        -- Pools events
        events <- SDL.pollEvents

        let handleInput = \case
              -- User requests quit
              SDL.QuitEvent -> (Any True, mempty)
              -- Handle key presses
              SDL.KeyboardEvent e ->
                (mempty,) $
                  if SDL.keyboardEventKeyMotion e == SDL.Pressed
                    then case SDL.keysymScancode $ SDL.keyboardEventKeysym e of
                      -- Increase alpha on w
                      SDL.ScancodeW -> Sum 32
                      -- Decrease alpha on s
                      SDL.ScancodeS -> Sum (-32)
                      _ -> mempty
                    else mempty
              _ -> mempty

        let (Any quit, Sum alphaAdjustment) = foldMap (handleInput . SDL.eventPayload) events

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render background
        renderTexture renderer backgroundTexture 0 Nothing

        -- Render front blended
        let alpha' = max 0 $ min 255 $ alpha + alphaAdjustment
        setTextureAlpha modulatedTexture $ fromIntegral alpha'
        renderTexture renderer modulatedTexture 0 Nothing

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit $ loop alpha'

  -- Start loop
  loop (255 :: Int)

  -- Free loaded images
  mapM_ freeTexture [modulatedTexture, backgroundTexture]

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
