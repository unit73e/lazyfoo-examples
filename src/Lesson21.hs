{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson21 (main) where

import Control.Monad hiding (mapM_)
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable hiding (elem)
import Data.Maybe
import Data.Monoid
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import qualified SDL.Mixer as Mix
import SDL.Vect
import Prelude hiding (any, mapM_)

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture ::
  SDL.Renderer ->
  Texture ->
  Point V2 CInt ->
  Maybe (SDL.Rectangle CInt) ->
  Maybe CDouble ->
  Maybe (Point V2 CInt) ->
  Maybe (V2 Bool) ->
  IO ()
renderTexture r (Texture t size) xy clip theta center flips =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
   in -- Render to screen
      SDL.copyEx
        r
        t
        clip
        (Just $ SDL.Rectangle xy dstSize)
        (fromMaybe 0 theta)
        center
        (fromMaybe (pure False) flips)

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

data SoundType = Chunk Mix.Chunk | Music Mix.Music | StopMusic

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo, SDL.InitAudio]
  Mix.openAudio Mix.defaultAudio 256

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create vsynced renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedVSyncRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load textures
  promptTexture <- loadTexture renderer "prompt.png"

  -- Load sound effects
  high <- Mix.load =<< getDataFileName "high.wav" :: IO Mix.Chunk
  medium <- Mix.load =<< getDataFileName "medium.wav" :: IO Mix.Chunk
  low <- Mix.load =<< getDataFileName "low.wav" :: IO Mix.Chunk
  scratch <- Mix.load =<< getDataFileName "scratch.wav" :: IO Mix.Chunk
  music <- Mix.load =<< getDataFileName "beat.wav" :: IO Mix.Music

  let loop = do
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit
        let quit = SDL.QuitEvent `elem` events

        let handleInput = \case
              -- User presses a key
              SDL.KeyboardEvent e | SDL.keyboardEventKeyMotion e == SDL.Pressed ->
                -- Select surfaces based on key press
                case SDL.keysymKeycode $ SDL.keyboardEventKeysym e of
                  SDL.Keycode1 -> Last $ Just $ Chunk high
                  SDL.Keycode2 -> Last $ Just $ Chunk medium
                  SDL.Keycode3 -> Last $ Just $ Chunk low
                  SDL.Keycode4 -> Last $ Just $ Chunk scratch
                  SDL.Keycode9 -> Last $ Just $ Music music
                  SDL.Keycode0 -> Last $ Just StopMusic
                  _ -> mempty
              _ -> mempty

        -- Set sound based on pressed key
        let currentSound = getLast $ foldMap handleInput events

        -- Handle sound type
        let handleSoundType = \case
              -- If sound effect, play
              Chunk a -> Mix.play a
              -- If music
              Music a ->
                Mix.playingMusic >>= \case
                  -- If the music is being played
                  True ->
                    Mix.pausedMusic >>= \case
                      -- If the music is paused, resume music
                      True -> Mix.resumeMusic
                      -- If the music is playing, pause music
                      False -> Mix.pauseMusic
                  -- If there is no music playing, play music
                  False -> Mix.playMusic Mix.Forever a
              -- Stop the music
              StopMusic -> Mix.haltMusic
        mapM_ handleSoundType currentSound

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render current texture
        renderTexture renderer promptTexture 0 Nothing Nothing Nothing Nothing

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit loop

  -- Start loop
  loop

  -- Free loaded images
  freeTexture promptTexture

  -- Close mix
  Mix.closeAudio

  -- Free sound effects
  mapM_ Mix.free [high, medium, low, scratch]

  -- Free music
  Mix.free music

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.Image.quit
  Mix.quit
  SDL.quit
