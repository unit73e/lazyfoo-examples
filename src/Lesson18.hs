{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson18 (main) where

import Control.Monad hiding (mapM_)
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable hiding (elem)
import Data.Maybe
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect
import Prelude hiding (any, mapM_)

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture ::
  SDL.Renderer ->
  Texture ->
  Point V2 CInt ->
  Maybe (SDL.Rectangle CInt) ->
  Maybe CDouble ->
  Maybe (Point V2 CInt) ->
  Maybe (V2 Bool) ->
  IO ()
renderTexture r (Texture t size) xy clip theta center flips =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
   in -- Render to screen
      SDL.copyEx
        r
        t
        clip
        (Just $ SDL.Rectangle xy dstSize)
        (fromMaybe 0 theta)
        center
        (fromMaybe (pure False) flips)

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create vsynced renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedVSyncRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load textures
  pressTexture <- loadTexture renderer "press.bmp"
  upTexture <- loadTexture renderer "up.bmp"
  downTexture <- loadTexture renderer "down.bmp"
  leftTexture <- loadTexture renderer "left.bmp"
  rightTexture <- loadTexture renderer "right.bmp"

  let loop = do
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit
        let quit = SDL.QuitEvent `elem` events

        -- Set texture based on current keystate
        keyMap <- SDL.getKeyboardState
        let texture =
              if
                  | keyMap SDL.ScancodeUp -> upTexture
                  | keyMap SDL.ScancodeDown -> downTexture
                  | keyMap SDL.ScancodeLeft -> leftTexture
                  | keyMap SDL.ScancodeRight -> rightTexture
                  | otherwise -> pressTexture

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render current texture
        renderTexture renderer texture 0 Nothing Nothing Nothing Nothing

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit loop

  -- Start loop
  loop

  -- Free loaded images
  mapM_ freeTexture [pressTexture, upTexture, downTexture, leftTexture, rightTexture]

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.Image.quit
  SDL.quit
