{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson17 (main) where

import Control.Monad
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable
import Data.Maybe
import Data.Monoid
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture ::
  SDL.Renderer ->
  Texture ->
  Point V2 CInt ->
  Maybe (SDL.Rectangle CInt) ->
  Maybe CDouble ->
  Maybe (Point V2 CInt) ->
  Maybe (V2 Bool) ->
  IO ()
renderTexture r (Texture t size) xy clip theta center flips =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
   in -- Render to screen
      SDL.copyEx
        r
        t
        clip
        (Just $ SDL.Rectangle xy dstSize)
        (fromMaybe 0 theta)
        center
        (fromMaybe (pure False) flips)

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

data ButtonSprite = MouseOut | MouseOver | MouseDown | MouseUp

data Button = Button (Point V2 CInt) ButtonSprite

buttonSize :: V2 CInt
buttonWidth, buttonHeight :: CInt
buttonSize@(V2 buttonWidth buttonHeight) = V2 300 200

handleEvent :: Point V2 CInt -> SDL.EventPayload -> Button -> Button
handleEvent mousePos ev (Button buttonPos _) =
  let inside =
        and ((>=) <$> mousePos <*> buttonPos)
          && and ((<=) <$> mousePos <*> buttonPos + P buttonSize)
      sprite
        | inside = case ev of
          SDL.MouseButtonEvent e
            | SDL.mouseButtonEventMotion e == SDL.Pressed -> MouseDown
            | SDL.mouseButtonEventMotion e == SDL.Released -> MouseUp
            | otherwise -> MouseOver
          _ -> MouseOver
        | otherwise = MouseOut
   in Button buttonPos sprite

renderButton :: SDL.Renderer -> Texture -> Button -> IO ()
renderButton r spriteSheet (Button xy sprite) =
  renderTexture r spriteSheet xy (Just spriteClipRect) Nothing Nothing Nothing
  where
    spriteClipRect =
      let i = case sprite of
            MouseOut -> 0
            MouseOver -> 1
            MouseDown -> 2
            MouseUp -> 3
       in SDL.Rectangle (P $ V2 0 $ i * 200) (V2 300 200)

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create vsynced renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedVSyncRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load sprites
  buttonSpriteSheet <- loadTexture renderer "button.png"

  let loop buttons = do
        events <- SDL.pollEvents
        mousePos <- SDL.getAbsoluteMouseLocation

        -- Handle button events
        let handleInput = \case
              SDL.QuitEvent -> (Any True, mempty)
              e -> (mempty, Endo $ handleEvent mousePos e)

        let (Any quit, Endo updateButton) =
              foldMap (handleInput . SDL.eventPayload) events

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render buttons
        let buttons' = map updateButton buttons
        for_ buttons' $ renderButton renderer buttonSpriteSheet

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit (loop buttons')

  -- Start loop
  loop $
    let newButton xy = Button xy MouseOut
     in [ newButton $ P $ V2 0 0,
          newButton $ P $ V2 (screenWidth - buttonWidth) 0,
          newButton $ P $ V2 0 (screenHeight - buttonHeight),
          newButton $ P $ V2 screenWidth screenHeight - buttonSize
        ]

  -- Free loaded images
  freeTexture buttonSpriteSheet

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
