{-# LANGUAGE OverloadedStrings #-}

module Lesson11 (main) where

import Control.Applicative
import Control.Monad
import Control.Monad.IO.Class (MonadIO)
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture :: SDL.Renderer -> Texture -> Point V2 CInt -> Maybe (SDL.Rectangle CInt) -> IO ()
renderTexture r (Texture t size) xy clip =
      -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      dstRect = Just $ SDL.Rectangle xy dstSize
      -- Render to screen
   in SDL.copy r t clip dstRect

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create renderer for window
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load sprite sheet texture
  spriteSheetTexture <- loadTexture renderer "dots.png"

  let spriteSize = V2 100 100
      -- Set top left sprite
      clip1 = SDL.Rectangle (P $ V2 0 0) spriteSize
      -- Set top right sprite
      clip2 = SDL.Rectangle (P $ V2 100 0) spriteSize
      -- Set bottom left sprite
      clip3 = SDL.Rectangle (P $ V2 0 100) spriteSize
      -- Set bottom right sprite
      clip4 = SDL.Rectangle (P $ V2 100 100) spriteSize

  let loop = do
        -- Pools events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        let position1 = P $ V2 0 0
            position2 = P $ V2 (screenWidth -) (const 0) <*> spriteSize
            position3 = P $ V2 (const 0) (screenHeight -) <*> spriteSize
            position4 = P $ V2 (screenWidth -) (screenHeight -) <*> spriteSize

        -- Render top left sprite
        renderTexture renderer spriteSheetTexture position1 (Just clip1)

        -- Render top right sprite
        renderTexture renderer spriteSheetTexture position2 (Just clip2)

        -- Render bottom left sprite
        renderTexture renderer spriteSheetTexture position3 (Just clip3)

        -- Render bottom right sprite
        renderTexture renderer spriteSheetTexture position4 (Just clip4)

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit loop

  -- Start loop
  loop

  -- Free loaded images
  freeTexture spriteSheetTexture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
