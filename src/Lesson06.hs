{-# LANGUAGE OverloadedStrings #-}

module Lesson06 (main) where

import Control.Applicative
import Control.Monad
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

-- Loads an image and optimizes to surface format
loadSurface :: SDL.Surface -> FilePath -> IO SDL.Surface
loadSurface screenSurface path = do
  -- Load image at specified path
  loadedSurface <- getDataFileName path >>= SDL.Image.load
  -- Get surface format
  desiredFormat <- SDL.surfaceFormat screenSurface
  -- Convert surface to screen format and get rid of old loaded surface
  SDL.convertSurface loadedSurface desiredFormat <* SDL.freeSurface loadedSurface

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Get window surface
  screenSurface <- SDL.getWindowSurface window

  -- Loads the image
  stretchedSurface <- loadSurface screenSurface "loaded.png"

  let loop = do
        -- Pools events
        events <- SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = elem SDL.QuitEvent $ map SDL.eventPayload events

        -- Apply the PNG image
        SDL.surfaceBlitScaled stretchedSurface Nothing screenSurface Nothing

        -- Updates the surface
        SDL.updateWindowSurface window

        -- Continue loop unless user requests quit
        unless quit loop

  -- Start loop
  loop

  -- Free loaded image
  SDL.freeSurface stretchedSurface

  -- Destroy window
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
