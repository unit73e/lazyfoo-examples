{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson15 (main) where

import Control.Applicative
import Control.Monad
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable
import Data.Maybe
import Data.Monoid
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture ::
  SDL.Renderer ->
  Texture ->
  Point V2 CInt ->
  Maybe (SDL.Rectangle CInt) ->
  Maybe CDouble ->
  Maybe (Point V2 CInt) ->
  Maybe (V2 Bool) ->
  IO ()
renderTexture r (Texture t size) xy clip theta center flips =
      -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      -- Render to screen
   in SDL.copyEx
        r
        t
        clip
        (Just $ SDL.Rectangle xy dstSize)
        (fromMaybe 0 theta)
        center
        (fromMaybe (pure False) flips)

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

textureSize :: Texture -> V2 CInt
textureSize (Texture _ sz) = sz

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create vsynced renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedVSyncRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load arrow texture
  arrowTexture <- loadTexture renderer "arrow.png"

  let loop theta flips = do
        events <- SDL.pollEvents

        -- Handle events on queue
        let handleInput = \case
              -- User requests quit
              SDL.QuitEvent -> (Any True, mempty, mempty)
              SDL.KeyboardEvent e ->
                (\(x, y) -> (mempty, x, y)) $
                  if SDL.keyboardEventKeyMotion e == SDL.Pressed
                    then case SDL.keysymScancode $ SDL.keyboardEventKeysym e of
                      SDL.ScancodeQ -> (mempty, Last $ Just $ V2 True False)
                      SDL.ScancodeW -> (mempty, Last $ Just $ V2 False False)
                      SDL.ScancodeE -> (mempty, Last $ Just $ V2 False True)
                      SDL.ScancodeA -> (Sum (-60), mempty)
                      SDL.ScancodeD -> (Sum 60, mempty)
                      _ -> mempty
                    else mempty
              _ -> mempty

        let (Any quit, Sum phi, Last newFlips) = foldMap (handleInput . SDL.eventPayload) events

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Renders arrow
        let theta' = theta + phi
            flips' = fromMaybe flips newFlips
            screenCenter = fmap (`div` 2) (V2 screenWidth screenHeight)
            origin = P $ screenCenter - fmap (`div` 2) (textureSize arrowTexture)
        renderTexture renderer arrowTexture origin Nothing (Just theta') Nothing (Just flips')

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit (loop theta' flips')

  -- Start loop
  loop 0 $ pure False

  -- Free loaded images
  freeTexture arrowTexture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
