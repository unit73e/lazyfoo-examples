{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Lesson12 (main) where

import Control.Monad
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable
import Data.Monoid
import Data.Word
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture :: SDL.Renderer -> Texture -> Point V2 CInt -> Maybe (SDL.Rectangle CInt) -> IO ()
renderTexture r (Texture t size) xy clip =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      dstRect = Just $ SDL.Rectangle xy dstSize
   in -- Render to screen
      SDL.copy r t clip dstRect

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

-- Modulate texture
setTextureColor :: Texture -> V3 Word8 -> IO ()
setTextureColor (Texture t _) rgb = SDL.textureColorMod t $= rgb

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create renderer for window
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Loads the color texture
  modulatedTexture <- loadTexture renderer "colors.png"

  let loop color = do
        -- Pools events
        events <- SDL.pollEvents

        let handleInput = \case
              SDL.QuitEvent -> (Any True, mempty)
              SDL.KeyboardEvent e ->
                (mempty,) $
                  --  On keypress change rgb values
                  if SDL.keyboardEventKeyMotion e == SDL.Pressed
                    then case SDL.keysymScancode $ SDL.keyboardEventKeysym e of
                      -- Increase red
                      SDL.ScancodeQ -> Sum (V3 32 0 0)
                      -- Increase blue
                      SDL.ScancodeW -> Sum (V3 0 32 0)
                      -- Increase green
                      SDL.ScancodeE -> Sum (V3 0 0 32)
                      -- Decrease red
                      SDL.ScancodeA -> Sum (V3 224 0 0)
                      -- Decrease blue
                      SDL.ScancodeS -> Sum (V3 0 224 0)
                      -- Decrease green
                      SDL.ScancodeD -> Sum (V3 0 0 224)
                      _ -> mempty
                    else mempty
              _ -> mempty

        let (Any quit, Sum colorAdjustment) = foldMap (handleInput . SDL.eventPayload) events

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Modulate and render texture
        let color' = color + colorAdjustment
        setTextureColor modulatedTexture color'
        renderTexture renderer modulatedTexture 0 Nothing

        -- Update screen
        SDL.present renderer
        
        -- Continue loop unless user requests quit
        unless quit (loop color')

  -- Start loop
  loop (V3 maxBound maxBound maxBound)

  -- Free loaded images
  freeTexture modulatedTexture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
