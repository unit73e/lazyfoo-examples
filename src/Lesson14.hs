{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson14 (main) where

import Control.Monad
import Control.Monad.IO.Class (MonadIO)
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture :: SDL.Renderer -> Texture -> Point V2 CInt -> Maybe (SDL.Rectangle CInt) -> IO ()
renderTexture r (Texture t size) xy clip =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      dstRect = Just $ SDL.Rectangle xy dstSize
   in -- Render to screen
      SDL.copy r t clip dstRect

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create vsynced renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedVSyncRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load sprite sheet texture
  spriteSheetTexture <- loadTexture renderer "animation.png"

  -- Set sprite clips
  let spriteSize = V2 64 205
      origin1 = P $ V2 0 0
      origin2 = P $ V2 64 0
      origin3 = P $ V2 128 0
      origin4 = P $ V2 196 0
      clip1 = SDL.Rectangle origin1 spriteSize
      clip2 = SDL.Rectangle origin2 spriteSize
      clip3 = SDL.Rectangle origin3 spriteSize
      clip4 = SDL.Rectangle origin4 spriteSize

  -- Sets the animation loop
  let loop [] = return ()
      loop (frame : frames) = do
        -- Pools events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events

        -- Clear window
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render current frame
        let position = P $ fmap (`div` 2) (V2 screenWidth screenHeight) - fmap (`div` 2) spriteSize
        renderTexture renderer spriteSheetTexture position $ Just frame

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit $ loop frames

  -- Start loop
  loop $ cycle $ [clip1, clip2, clip3, clip4] >>= replicate 4

  -- Free loaded images
  freeTexture spriteSheetTexture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
