{-# LANGUAGE OverloadedStrings #-}

module Lesson08 (main) where

import Control.Monad
import Data.Foldable (for_)
import Foreign.C.Types
import SDL (($=))
import qualified SDL
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Creates an hardware accelerated renderer
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  let loop = do
        -- Pools events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events

        -- Clear screen
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render red filled quad
        let redRectStart = P $ V2 (screenWidth `div` 4) (screenHeight `div` 4)
        let redRectEnd = V2 (screenWidth `div` 2) (screenHeight `div` 2)
        let redColor = V4 maxBound 0 0 maxBound
        SDL.rendererDrawColor renderer $= redColor
        SDL.fillRect renderer $ Just $ SDL.Rectangle redRectStart redRectEnd

        -- Render green outlined quad
        let greenRectStart = P $ V2 (screenWidth `div` 6) (screenHeight `div` 6)
        let greenRectEnd = V2 (screenWidth * 2 `div` 3) (screenHeight * 2 `div` 3)
        let greenColor = V4 0 maxBound 0 maxBound
        SDL.rendererDrawColor renderer $= greenColor
        SDL.drawRect renderer $ Just $ SDL.Rectangle greenRectStart greenRectEnd

        -- Render blue horizontal line
        let blueLineStart = P $ V2 0 (screenHeight `div` 2)
        let blueLineEnd = P $ V2 screenWidth (screenHeight `div` 2)
        let blueColor = V4 0 0 maxBound maxBound
        SDL.rendererDrawColor renderer $= blueColor
        SDL.drawLine renderer blueLineStart blueLineEnd

        -- Draw vertical line of yellow dots
        let yellowColor = V4 maxBound maxBound 0 maxBound
        SDL.rendererDrawColor renderer $= yellowColor
        for_ [0, 4 .. screenHeight] $ \i -> do
          let point = P $ V2 (screenWidth `div` 2) i
          SDL.drawPoint renderer point

        -- Update screen
        SDL.present renderer

        -- Continue loop unless user requests quit
        unless quit loop

  -- Start loop
  loop

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
