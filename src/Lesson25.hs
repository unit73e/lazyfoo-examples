{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson25 (main) where

import Control.Lens
import Control.Monad hiding (mapM_)
import Control.Monad.IO.Class (MonadIO)
import Control.Concurrent
import Data.Foldable hiding (elem)
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import Data.Word
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Font
import SDL.Vect
import Prelude hiding (any, mapM_)

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

screenFps = 60

screenTickPerFrame = 1000 `div` screenFps

data Texture = Texture SDL.Texture (V2 CInt)

black = V4 0 0 0 0

loadText :: SDL.Renderer -> SDL.Font.Color -> T.Text -> IO Texture
loadText r color text = do
  -- Open the font
  fontPath <- getDataFileName "lazy.ttf"
  font <- SDL.Font.load fontPath 28
  -- Render text
  surface <- SDL.Font.solid font color text
  -- Free font
  SDL.Font.free font
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture :: SDL.Renderer -> Texture -> Point V2 CInt -> Maybe (SDL.Rectangle CInt) -> IO ()
renderTexture r (Texture t size) xy clip =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      dstRect = Just $ SDL.Rectangle xy dstSize
   in -- Render to screen
      SDL.copy r t clip dstRect

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

textureSize :: Texture -> V2 CInt
textureSize (Texture _ sz) = sz

data TimerActions = Stop | Start | Pause | Resume

data Timer = Timer
  { started :: Bool,
    paused :: Bool,
    startTicks :: Word32,
    pauseTicks :: Word32
  }

start timer ticks = Timer True False ticks 0

stop timer ticks = Timer False False 0 0

pause timer ticks =
  if started timer && not (paused timer)
    then Timer True True 0 (ticks - startTicks timer)
    else timer

unpause timer ticks =
  if started timer && paused timer
    then Timer True False (ticks - pauseTicks timer) 0
    else timer

getTicks timer ticks =
  if started timer
    then
      if paused timer
        then pauseTicks timer
        else ticks - startTicks timer
    else 0

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Initialize SDL font
  SDL.Font.initialize

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create accelerated renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  ticks <- SDL.ticks
  let timer = start (Timer False False 0 0) ticks

  let loop countedFrames capTimer = do
        ticks' <- SDL.ticks
        let capTimer' = start capTimer ticks'

        -- Pool events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events


        -- Calculate fps
        ticks'' <- SDL.ticks
        let avgFPS = fromIntegral countedFrames / (fromIntegral (getTicks timer ticks'') / 1000) :: Float

        -- Set text to be rendered
        let timeText = "Average Frames Per Second (With Cap)" `T.append` (T.pack . show) avgFPS

        -- Clears the window
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render text
        fpsTextTexture <- loadText renderer black timeText
        let textSize = textureSize fpsTextTexture
        let position = P $ fmap (`div` 2) (V2 screenWidth screenHeight) - fmap (`div` 2) textSize
        renderTexture renderer fpsTextTexture position Nothing

        -- Update screen
        SDL.present renderer

        -- Free time texture
        freeTexture fpsTextTexture

        -- Continue loop unless user requests quit
        ticks''' <- SDL.ticks
        let frameTicks = getTicks capTimer' ticks'''
        when (frameTicks < screenTickPerFrame) $
          SDL.delay $ fromIntegral $ screenTickPerFrame - frameTicks

        unless quit $ loop (countedFrames + 1) capTimer'

  -- Start loop
  loop 0 timer

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.Font.quit
  SDL.quit
