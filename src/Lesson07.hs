{-# LANGUAGE OverloadedStrings #-}

module Lesson07 (main) where

import Control.Monad
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Sets texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Creates renderer for window
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  -- Load image at specified path
  xOutSurface <- getDataFileName "texture.png" >>= SDL.Image.load

  -- Create texture from surface pixels
  texture <- SDL.createTextureFromSurface renderer xOutSurface

  -- Get rid of old loaded surface
  SDL.freeSurface xOutSurface

  -- Shows a texture until the user closes the window
  let loop = do
        -- Pools events
        events <- map SDL.eventPayload <$> SDL.pollEvents

        -- True if user requests quit or false otherwise
        let quit = SDL.QuitEvent `elem` events

        -- Clear screen
        SDL.clear renderer

        -- Render texture to screen
        SDL.copy renderer texture Nothing Nothing

        -- Update screen
        SDL.present renderer

        -- Continue loop until user requests quit
        unless quit loop

  -- Start loop
  loop
  
  -- Free loaded image
  SDL.destroyTexture texture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
