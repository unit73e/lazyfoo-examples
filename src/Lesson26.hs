{-# LANGUAGE CPP #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module Lesson26 (main) where

import Control.Concurrent
import Control.Lens
import Control.Monad hiding (mapM_)
import Control.Monad.IO.Class (MonadIO)
import Data.Foldable hiding (elem)
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import Data.Word
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Font
import qualified SDL.Image
import SDL.Vect
import Prelude hiding (any, mapM_)

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

screenFps = 60

screenTickPerFrame = 1000 `div` screenFps

data Texture = Texture SDL.Texture (V2 CInt)

cyan = V4 0 maxBound maxBound maxBound

loadTexture :: SDL.Renderer -> FilePath -> IO Texture
loadTexture r filePath = do
  -- Load image at specified path
  surface <- getDataFileName filePath >>= SDL.Image.load
  -- Color key image
  SDL.surfaceColorKey surface $= Just cyan
  -- Create texture from surface pixels
  t <- SDL.createTextureFromSurface r surface
  -- Get image dimensions
  size <- SDL.surfaceDimensions surface
  -- Get rid of old loaded surface
  SDL.freeSurface surface
  return $ Texture t size

renderTexture :: SDL.Renderer -> Texture -> Point V2 CInt -> Maybe (SDL.Rectangle CInt) -> IO ()
renderTexture r (Texture t size) xy clip =
  -- Set rendering space
  let dstSize = maybe size (\(SDL.Rectangle _ size') -> size') clip
      dstRect = Just $ SDL.Rectangle xy dstSize
   in -- Render to screen
      SDL.copy r t clip dstRect

freeTexture :: MonadIO m => Texture -> m ()
freeTexture (Texture t s) = SDL.destroyTexture t

textureSize :: Texture -> V2 CInt
textureSize (Texture _ sz) = sz

data Dot = Dot
  { posX :: CInt,
    posY :: CInt,
    velX :: CInt,
    velY :: CInt
  }

dotWidth, dotHeight :: CInt
(dotWidth, dotHeight) = (20, 20)

dotVel :: CInt
dotVel = 10

move :: Dot -> Dot
move (Dot posX posY velX velY) = Dot posX' posY' velX velY
  where
    posX' =
      if posX + velX < 0 || posX + velX + dotWidth > screenWidth
        then posX
        else posX + velX
    posY' =
      if posY + velY < 0 || posY + velY + dotHeight > screenHeight
        then posY
        else posY + velY

handleEvents d =
  foldMap (handleEvent d . SDL.eventPayload)
  where
    handleEvent d e = case e of
      SDL.QuitEvent -> (Any True, mempty)
      SDL.KeyboardEvent e -> (mempty, handleKeyboardEvent' d e)
      _ -> mempty
    handleKeyboardEvent' d e
      | SDL.keyboardEventKeyMotion e == SDL.Pressed && not (SDL.keyboardEventRepeat e) =
        case SDL.keysymScancode $ SDL.keyboardEventKeysym e of
          SDL.ScancodeUp -> Last $ Just $ changeVelocity d 0 (- dotVel)
          SDL.ScancodeDown -> Last $ Just $ changeVelocity d 0 dotVel
          SDL.ScancodeLeft -> Last $ Just $ changeVelocity d (- dotVel) 0
          SDL.ScancodeRight -> Last $ Just $ changeVelocity d dotVel 0
          _ -> mempty
      | SDL.keyboardEventKeyMotion e == SDL.Released && not (SDL.keyboardEventRepeat e) =
        case SDL.keysymScancode $ SDL.keyboardEventKeysym e of
          SDL.ScancodeUp -> Last $ Just $ changeVelocity d 0 dotVel
          SDL.ScancodeDown -> Last $ Just $ changeVelocity d 0 (- dotVel)
          SDL.ScancodeLeft -> Last $ Just $ changeVelocity d dotVel 0
          SDL.ScancodeRight -> Last $ Just $ changeVelocity d (- dotVel) 0
          _ -> mempty
      | otherwise = mempty
    changeVelocity (Dot posX posY velX velY) x y =
      Dot posX posY (velX + x) (velY + y)

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Initialize SDL font
  SDL.Font.initialize

  -- Set texture filtering to linear
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do
    renderQuality <- SDL.get SDL.HintRenderScaleQuality
    when (renderQuality /= SDL.ScaleLinear) $
      putStrLn "Warning: Linear texture filtering not enabled!"

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Create accelerated renderer for window
  renderer <-
    SDL.createRenderer
      window
      (-1)
      SDL.RendererConfig
        { SDL.rendererType = SDL.AcceleratedVSyncRenderer,
          SDL.rendererTargetTexture = False
        }

  -- Initialize renderer color
  SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound

  dotTexture <- loadTexture renderer "dot.bmp"

  let loop d = do
        -- Pool events
        events <- SDL.pollEvents

        -- Handle events
        let (Any quit, Last d') = handleEvents d events
        let d'' = maybe d move d'

        -- Clears the window
        SDL.rendererDrawColor renderer $= V4 maxBound maxBound maxBound maxBound
        SDL.clear renderer

        -- Render objects
        let pos = P $ V2 (posX d'') (posY d'')
        renderTexture renderer dotTexture pos Nothing

        -- Update screen
        SDL.present renderer

        unless quit $ loop d''

  -- Start loop
  loop $ Dot 0 0 0 0

  -- Free dot texture
  freeTexture dotTexture

  -- Destroy window
  SDL.destroyRenderer renderer
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.Image.quit
  SDL.quit
