{-# LANGUAGE OverloadedStrings #-}

module Lesson02 (main) where

import Control.Concurrent (threadDelay)
import Foreign.C.Types
import Paths_lazyfoo_examples (getDataFileName)
import qualified SDL
import SDL.Vect

-- Screen dimension constants
screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

main :: IO ()
main = do
  -- Initialize SDL
  SDL.initialize [SDL.InitVideo]

  -- Create window
  let windowConfig = SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight}
  let windowTitle = "SDL Tutorial"
  window <- SDL.createWindow windowTitle windowConfig

  -- Get window surface
  screenSurface <- SDL.getWindowSurface window

  -- Load splash image
  helloWorld <- getDataFileName "hello_world.bmp" >>= SDL.loadBMP

  -- Apply the image
  SDL.surfaceBlit helloWorld Nothing screenSurface Nothing

  -- Update the surface
  SDL.updateWindowSurface window

  -- Wait 2 seconds
  threadDelay 2000000

  -- Deallocate surface
  SDL.freeSurface helloWorld

  -- Destroy window
  SDL.destroyWindow window

  -- Quit SDL subsystems
  SDL.quit
