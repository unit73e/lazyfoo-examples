# Lazyfoo SDL2 Haskell 

A port of [lazyfoo SDL2 tutorial][lazyfoo] in Haskell. Most of the port is
based on [SDL2 library port][sdl2-lazyfoo], with missing examples completed.

The goal is for the code to be as similar to the original source code as much
possible, comments included. The lessons are stored in `src/LessonNN.hs` as a
single file per lesson.

## Build

To build:

```
stack build
```

## Usage

To run a lesson:

```
stack run -- <NN>
```

Where NN is the lesson number (e.g., 01, 02).

[lazyfoo]: https://lazyfoo.net/tutorials/SDL/index.php
[sdl2-lazyfoo]: https://github.com/haskell-game/sdl2/tree/master/examples/lazyfoo
