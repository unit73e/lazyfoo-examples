module Main where

import qualified Lesson01
import qualified Lesson02
import qualified Lesson03
import qualified Lesson04
import qualified Lesson05
import qualified Lesson06
import qualified Lesson07
import qualified Lesson08
import qualified Lesson09
import qualified Lesson10
import qualified Lesson11
import qualified Lesson12
import qualified Lesson13
import qualified Lesson14
import qualified Lesson15
import qualified Lesson16
import qualified Lesson17
import qualified Lesson18
import qualified Lesson19
import qualified Lesson21
import qualified Lesson22
import qualified Lesson23
import qualified Lesson24
import qualified Lesson25
import qualified Lesson26
import System.Environment (getArgs)

usage :: IO()
usage = putStrLn "lazyfoo-examples <NN>"

run :: String -> IO()
run "01" = Lesson01.main
run "02" = Lesson02.main
run "03" = Lesson03.main
run "04" = Lesson04.main
run "05" = Lesson05.main
run "06" = Lesson06.main
run "07" = Lesson07.main
run "08" = Lesson08.main
run "09" = Lesson09.main
run "10" = Lesson10.main
run "11" = Lesson11.main
run "12" = Lesson12.main
run "13" = Lesson13.main
run "14" = Lesson14.main
run "15" = Lesson15.main
run "16" = Lesson16.main
run "17" = Lesson17.main
run "18" = Lesson18.main
run "19" = Lesson19.main
run "21" = Lesson21.main
run "22" = Lesson22.main
run "23" = Lesson23.main
run "24" = Lesson24.main
run "25" = Lesson25.main
run "26" = Lesson26.main
run _    = usage

main :: IO ()
main = do
  args <- getArgs
  if null args then usage else run $ head args
